package ru.t1.nkiryukhin.tm.api.component;

import ru.t1.nkiryukhin.tm.dto.request.AbstractRequest;
import ru.t1.nkiryukhin.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request) throws Exception;

}
