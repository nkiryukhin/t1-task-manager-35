package ru.t1.nkiryukhin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.api.repository.ITaskRepository;
import ru.t1.nkiryukhin.tm.api.service.ITaskService;
import ru.t1.nkiryukhin.tm.comparator.NameComparator;
import ru.t1.nkiryukhin.tm.enumerated.Sort;
import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.entity.TaskNotFoundException;
import ru.t1.nkiryukhin.tm.exception.field.*;
import ru.t1.nkiryukhin.tm.exception.user.AccessDeniedException;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;
import ru.t1.nkiryukhin.tm.model.Task;
import ru.t1.nkiryukhin.tm.repository.TaskRepository;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

import static ru.t1.nkiryukhin.tm.data.ProjectTestData.USUAL_PROJECT1;
import static ru.t1.nkiryukhin.tm.data.TaskTestData.*;
import static ru.t1.nkiryukhin.tm.data.UserTestData.ADMIN_USER;
import static ru.t1.nkiryukhin.tm.data.UserTestData.USUAL_USER;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @NotNull
    private final ITaskRepository repository = new TaskRepository();

    @NotNull
    private final ITaskService service = new TaskService(repository);

    @Before
    public void before() {
        repository.add(USUAL_TASK1);
        repository.add(USUAL_TASK2);
    }

    @After
    public void after() throws AccessDeniedException {
        repository.removeAll(TASK_LIST);
    }

    @Test
    public void add() throws AbstractFieldException {
        Assert.assertNull(service.add(NULL_TASK));
        Assert.assertNotNull(service.add(ADMIN_TASK1));
        @Nullable final Task task = service.findOneById(ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1, task);
    }

    @Test
    public void addByUserId() throws AbstractFieldException {
        Assert.assertNull(service.add(ADMIN_USER.getId(), NULL_TASK));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.add(null, ADMIN_TASK1);
        });
        Assert.assertNotNull(service.add(ADMIN_USER.getId(), ADMIN_TASK1));
        @Nullable final Task task = service.findOneById(ADMIN_USER.getId(), ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1, task);
    }

    @Test
    public void addMany() throws AbstractFieldException {
        Assert.assertNotNull(service.add(ADMIN_TASK_LIST));
        for (final Task task : ADMIN_TASK_LIST)
            Assert.assertEquals(task, service.findOneById(task.getId()));
    }

    @Test
    public void set() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        @NotNull final ITaskService emptyService = new TaskService(emptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USUAL_TASK_LIST);
        emptyService.set(ADMIN_TASK_LIST);
        Assert.assertEquals(ADMIN_TASK_LIST, emptyService.findAll());
    }

    @Test
    public void findAll() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        @NotNull final ITaskService emptyService = new TaskService(emptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USUAL_TASK_LIST);
        Assert.assertEquals(USUAL_TASK_LIST, emptyService.findAll());
    }

    @Test
    public void findAllByUserId() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAll("");
        });
        Assert.assertEquals(USUAL_TASK_LIST, service.findAll(USUAL_USER.getId()));
    }

    @Test
    public void findAllComparator() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        @NotNull final ITaskService emptyService = new TaskService(emptyRepository);
        emptyService.add(USUAL_TASK_LIST);
        emptyService.add(ADMIN_TASK_LIST);
        @Nullable Comparator comparator = null;
        Assert.assertEquals(TASK_LIST, emptyService.findAll(comparator));
        comparator = NameComparator.INSTANCE;
        Assert.assertEquals(SORTED_TASK_LIST, emptyService.findAll(comparator));
    }

    @Test
    public void findAllComparatorByUserId() throws UserIdEmptyException {
        @Nullable Comparator comparator = null;
        Assert.assertEquals(USUAL_TASK_LIST, service.findAll(USUAL_USER.getId(), comparator));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Comparator comparatorInner = null;
            service.findAll("", comparatorInner);
        });
        comparator = NameComparator.INSTANCE;
        Assert.assertEquals(USUAL_TASK_LIST.stream().sorted(comparator).collect(Collectors.toList()), service.findAll(USUAL_USER.getId(), comparator));
    }

    @Test
    public void findAllSortByUserId() throws UserIdEmptyException {
        @Nullable Sort sort = null;
        Assert.assertEquals(USUAL_TASK_LIST, service.findAll(USUAL_USER.getId(), sort));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Sort sortInner = null;
            service.findAll("", sortInner);
        });
        sort = Sort.BY_NAME;
        Assert.assertEquals(USUAL_TASK_LIST.stream().sorted(sort.getComparator()).collect(Collectors.toList()), service.findAll(USUAL_USER.getId(), sort.getComparator()));
    }

    @Test
    public void existsById() {
        Assert.assertFalse(service.existsById(""));
        Assert.assertFalse(service.existsById(null));
        Assert.assertFalse(service.existsById(NON_EXISTING_TASK_ID));
        Assert.assertTrue(service.existsById(USUAL_TASK1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws AbstractFieldException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, NON_EXISTING_TASK_ID);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", NON_EXISTING_TASK_ID);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.existsById(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.existsById(USUAL_USER.getId(), "");
        });
        Assert.assertFalse(service.existsById(USUAL_USER.getId(), NON_EXISTING_TASK_ID));
        Assert.assertTrue(service.existsById(USUAL_USER.getId(), USUAL_TASK1.getId()));
    }

    @Test
    public void findOneById() throws AbstractFieldException {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById("");
        });
        Assert.assertNull(service.findOneById(NON_EXISTING_TASK_ID));
        @Nullable final Task task = service.findOneById(USUAL_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USUAL_TASK1, task);
    }

    @Test
    public void findOneByIdByUserId() throws AbstractFieldException {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USUAL_USER.getId(), "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, USUAL_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", USUAL_TASK1.getId());
        });
        Assert.assertNull(service.findOneById(USUAL_USER.getId(), NON_EXISTING_TASK_ID));
        @Nullable final Task task = service.findOneById(USUAL_USER.getId(), USUAL_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USUAL_TASK1, task);
    }

    @Test
    public void findOneByIndex() throws AbstractFieldException {
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(-1);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(service.getSize());
        });
        final int index = service.findAll().indexOf(USUAL_TASK1);
        @Nullable final Task task = service.findOneByIndex(index);
        Assert.assertNotNull(task);
        Assert.assertEquals(USUAL_TASK1, task);
    }

    @Test
    public void findOneByIndexByUserId() throws AbstractFieldException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneByIndex(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneByIndex("", null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(USUAL_USER.getId(), -1);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(USUAL_USER.getId(), service.getSize());
        });
        final int index = service.findAll().indexOf(USUAL_TASK1);
        @Nullable final Task task = service.findOneByIndex(USUAL_USER.getId(), index);
        Assert.assertNotNull(task);
        Assert.assertEquals(USUAL_TASK1, task);
    }

    @Test
    public void clear() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        @NotNull final ITaskService emptyService = new TaskService(emptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USUAL_TASK_LIST);
        emptyService.clear();
        Assert.assertEquals(0, emptyService.getSize());
    }

    @Test
    public void clearByUserId() throws UserIdEmptyException {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        @NotNull final ITaskService emptyService = new TaskService(emptyRepository);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            emptyService.clear(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            emptyService.clear("");
        });
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USUAL_TASK1);
        emptyService.add(USUAL_TASK2);
        emptyService.clear(USUAL_USER.getId());
        Assert.assertEquals(0, emptyService.getSize(USUAL_USER.getId()));
    }

    @Test
    public void remove() throws AbstractFieldException, AccessDeniedException {
        Assert.assertNull(service.removeOne(null));
        @Nullable final Task createdTask = service.add(ADMIN_TASK1);
        @Nullable final Task removedTask = service.removeOne(createdTask);
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(service.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeByUserId() throws AbstractFieldException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove("", null);
        });
        Assert.assertNull(service.remove(ADMIN_USER.getId(), null));
        @Nullable final Task createdTask = service.add(ADMIN_TASK1);
        @Nullable final Task removedTask = service.remove(ADMIN_USER.getId(), createdTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(service.findOneById(ADMIN_USER.getId(), ADMIN_TASK1.getId()));
    }

    @Test
    public void removeById() throws AbstractFieldException {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById("");
        });
        Assert.assertNull(service.removeById(NON_EXISTING_TASK_ID));
        service.add(ADMIN_TASK1);
        @Nullable final Task removedTask = service.removeById(ADMIN_TASK1.getId());
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(service.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeByIdByUserId() throws AbstractFieldException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById("", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USUAL_USER.getId(), "");
        });
        Assert.assertNull(service.removeById(ADMIN_USER.getId(), NON_EXISTING_TASK_ID));
        Assert.assertNull(service.removeById(ADMIN_USER.getId(), USUAL_TASK1.getId()));
        @Nullable final Task createdTask = service.add(ADMIN_TASK1);
        @Nullable final Task removedTask = service.removeById(ADMIN_USER.getId(), createdTask.getId());
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(service.findOneById(ADMIN_USER.getId(), ADMIN_TASK1.getId()));
    }

    @Test
    public void removeByIndex() throws AbstractFieldException {
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(-1);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(service.getSize());
        });
        @Nullable final Task createdTask = service.add(ADMIN_TASK1);
        final int index = service.findAll().indexOf(createdTask);
        @Nullable final Task removedTask = service.removeByIndex(index);
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(service.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeByIndexByUserId() throws AbstractFieldException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeByIndex(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeByIndex("", null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(USUAL_USER.getId(), -1);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(USUAL_USER.getId(), service.getSize());
        });
        @Nullable final Task createdTask = service.add(ADMIN_TASK1);
        final int index = service.findAll(ADMIN_USER.getId()).indexOf(createdTask);
        @Nullable final Task removedTask = service.removeByIndex(ADMIN_USER.getId(), index);
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(service.findOneById(ADMIN_USER.getId(), ADMIN_TASK1.getId()));
    }

    @Test
    public void getSize() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        @NotNull final ITaskService emptyService = new TaskService(emptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        Assert.assertEquals(0, emptyService.getSize());
        emptyService.add(ADMIN_TASK1);
        Assert.assertEquals(1, emptyService.getSize());
    }

    @Test
    public void getSizeByUserId() throws UserIdEmptyException {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        @NotNull final ITaskService emptyService = new TaskService(emptyRepository);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            emptyService.getSize(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            emptyService.getSize("");
        });
        Assert.assertTrue(emptyService.findAll().isEmpty());
        Assert.assertEquals(0, emptyService.getSize(ADMIN_USER.getId()));
        emptyService.add(ADMIN_TASK1);
        emptyService.add(USUAL_TASK1);
        Assert.assertEquals(1, emptyService.getSize(ADMIN_USER.getId()));
    }

    @Test
    public void removeAll() throws AccessDeniedException {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        @NotNull final ITaskService emptyService = new TaskService(emptyRepository);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            String userId = null;
            emptyService.removeAll(userId);
        });
        Assert.assertThrows(AccessDeniedException.class, () -> {
            emptyService.removeAll(Collections.emptyList());
        });
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(TASK_LIST);
        emptyService.removeAll(TASK_LIST);
        Assert.assertEquals(0, emptyService.getSize());
    }

    @Test
    public void create() throws AbstractFieldException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, ADMIN_TASK1.getName());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create("", ADMIN_TASK1.getName());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(ADMIN_USER.getId(), null);
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(ADMIN_USER.getId(), "");
        });
        @NotNull final Task task = service.create(ADMIN_USER.getId(), ADMIN_TASK1.getName());
        Assert.assertEquals(task, service.findOneById(ADMIN_USER.getId(), task.getId()));
        Assert.assertEquals(ADMIN_TASK1.getName(), task.getName());
        Assert.assertEquals(ADMIN_USER.getId(), task.getUserId());
    }

    @Test
    public void createWithDescription() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, ADMIN_TASK1.getName(), ADMIN_TASK1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create("", ADMIN_TASK1.getName(), ADMIN_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(ADMIN_USER.getId(), null, ADMIN_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(ADMIN_USER.getId(), "", ADMIN_TASK1.getDescription());
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.create(ADMIN_USER.getId(), ADMIN_TASK1.getName(), null);
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.create(ADMIN_USER.getId(), ADMIN_TASK1.getName(), "");
        });
        @NotNull final Task task = service.create(ADMIN_USER.getId(), ADMIN_TASK1.getName(), ADMIN_TASK1.getDescription());
        Assert.assertEquals(task, service.findOneById(ADMIN_USER.getId(), task.getId()));
        Assert.assertEquals(ADMIN_TASK1.getName(), task.getName());
        Assert.assertEquals(ADMIN_TASK1.getDescription(), task.getDescription());
        Assert.assertEquals(ADMIN_USER.getId(), task.getUserId());
    }


    @Test
    public void findAllByProjectId() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAllByProjectId(null, USUAL_PROJECT1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAllByProjectId("", USUAL_PROJECT1.getId());
        });
        @NotNull final Collection<Task> emptyCollection = Collections.emptyList();
        Assert.assertEquals(emptyCollection, service.findAllByProjectId(USUAL_USER.getId(), null));
        Assert.assertEquals(emptyCollection, service.findAllByProjectId(USUAL_USER.getId(), ""));
        Assert.assertEquals(USUAL_TASK_LIST, service.findAllByProjectId(USUAL_USER.getId(), USUAL_PROJECT1.getId()));
    }

    @Test
    public void updateById() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById(null, USUAL_TASK1.getId(), USUAL_TASK1.getName(), USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById("", USUAL_TASK1.getId(), USUAL_TASK1.getName(), USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(USUAL_USER.getId(), null, USUAL_TASK1.getName(), USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(USUAL_USER.getId(), "", USUAL_TASK1.getName(), USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(USUAL_USER.getId(), USUAL_TASK1.getId(), null, USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(USUAL_USER.getId(), USUAL_TASK1.getId(), "", USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.updateById(USUAL_USER.getId(), NON_EXISTING_TASK_ID, USUAL_TASK1.getName(), USUAL_TASK1.getDescription());
        });
        @NotNull final String name = USUAL_TASK1.getName() + NON_EXISTING_TASK_ID;
        @NotNull final String description = USUAL_TASK1.getDescription() + NON_EXISTING_TASK_ID;
        service.updateById(USUAL_USER.getId(), USUAL_TASK1.getId(), name, description);
        Assert.assertEquals(name, USUAL_TASK1.getName());
        Assert.assertEquals(description, USUAL_TASK1.getDescription());
    }

    @Test
    public void updateByIndex() throws AbstractException {
        @Nullable final Task task = service.findOneById(USUAL_TASK1.getId());
        Assert.assertNotNull(task);
        final int index = service.findAll().indexOf(task);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateByIndex(null, index, USUAL_TASK1.getName(), USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateByIndex("", index, USUAL_TASK1.getName(), USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.updateByIndex(USUAL_USER.getId(), null, USUAL_TASK1.getName(), USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.updateByIndex(USUAL_USER.getId(), -1, USUAL_TASK1.getName(), USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.updateByIndex(USUAL_USER.getId(), service.getSize(), USUAL_TASK1.getName(), USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateByIndex(USUAL_USER.getId(), index, null, USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateByIndex(USUAL_USER.getId(), index, "", USUAL_TASK1.getDescription());
        });
        @NotNull final String name = USUAL_TASK1.getName() + NON_EXISTING_TASK_ID;
        @NotNull final String description = USUAL_TASK1.getDescription() + NON_EXISTING_TASK_ID;
        service.updateByIndex(USUAL_USER.getId(), index, name, description);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

    @Test
    public void changeTaskStatusById() throws AbstractException {
        @NotNull final Status status = Status.COMPLETED;
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeTaskStatusById(null, USUAL_TASK1.getId(), status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeTaskStatusById("", USUAL_TASK1.getId(), status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeTaskStatusById(USUAL_USER.getId(), null, status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeTaskStatusById(USUAL_USER.getId(), "", status);
        });
        Assert.assertThrows(StatusEmptyException.class, () -> {
            service.changeTaskStatusById(USUAL_USER.getId(), USUAL_TASK1.getId(), null);
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.changeTaskStatusById(USUAL_USER.getId(), NON_EXISTING_TASK_ID, status);
        });
        service.changeTaskStatusById(USUAL_USER.getId(), USUAL_TASK1.getId(), status);
        Assert.assertNotNull(USUAL_TASK1);
        Assert.assertEquals(status, USUAL_TASK1.getStatus());
    }

    @Test
    public void changeTaskStatusByIndex() throws AbstractException {
        @NotNull final Status status = Status.COMPLETED;
        @Nullable final Task task = service.findOneById(USUAL_TASK1.getId());
        Assert.assertNotNull(task);
        final int index = service.findAll().indexOf(task);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeTaskStatusByIndex(null, index, status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeTaskStatusByIndex("", index, status);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.changeTaskStatusByIndex(USUAL_USER.getId(), null, status);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.changeTaskStatusByIndex(USUAL_USER.getId(), -1, status);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.changeTaskStatusByIndex(USUAL_USER.getId(), service.getSize(), status);
        });
        Assert.assertThrows(StatusEmptyException.class, () -> {
            service.changeTaskStatusByIndex(USUAL_USER.getId(), index, null);
        });
        service.changeTaskStatusByIndex(USUAL_USER.getId(), index, status);
        Assert.assertEquals(status, task.getStatus());
    }

}
