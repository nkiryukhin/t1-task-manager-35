package ru.t1.nkiryukhin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.api.repository.IProjectRepository;
import ru.t1.nkiryukhin.tm.api.repository.ITaskRepository;
import ru.t1.nkiryukhin.tm.api.service.IProjectTaskService;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.nkiryukhin.tm.exception.entity.TaskNotFoundException;
import ru.t1.nkiryukhin.tm.exception.field.ProjectIdEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.TaskIdEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;
import ru.t1.nkiryukhin.tm.exception.user.AccessDeniedException;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;
import ru.t1.nkiryukhin.tm.repository.ProjectRepository;
import ru.t1.nkiryukhin.tm.repository.TaskRepository;

import static ru.t1.nkiryukhin.tm.data.ProjectTestData.*;
import static ru.t1.nkiryukhin.tm.data.TaskTestData.*;
import static ru.t1.nkiryukhin.tm.data.UserTestData.USUAL_USER;

@Category(UnitCategory.class)
public class ProjectTaskServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectTaskService service = new ProjectTaskService(projectRepository, taskRepository);

    @Before
    public void before() {
        projectRepository.add(USUAL_PROJECT1);
        projectRepository.add(USUAL_PROJECT2);
        taskRepository.add(USUAL_TASK1);
        taskRepository.add(USUAL_TASK2);
    }

    @After
    public void after() throws AccessDeniedException {
        taskRepository.removeAll(TASK_LIST);
        projectRepository.removeAll(PROJECT_LIST);
    }

    @Test
    public void bindTaskToProject() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.bindTaskToProject(null, USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.bindTaskToProject("", USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.bindTaskToProject(USUAL_USER.getId(), null, USUAL_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.bindTaskToProject(USUAL_USER.getId(), "", USUAL_TASK1.getId());
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), null);
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.bindTaskToProject(USUAL_USER.getId(), NON_EXISTING_PROJECT_ID, USUAL_TASK1.getId());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), NON_EXISTING_TASK_ID);
        });
        service.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT2.getId(), USUAL_TASK1.getId());
        Assert.assertEquals(USUAL_PROJECT2.getId(), USUAL_TASK1.getProjectId());
        service.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
    }

    @Test
    public void removeProjectById() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeProjectById(null, USUAL_PROJECT1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeProjectById("", USUAL_PROJECT1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.removeProjectById(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.removeProjectById(USUAL_USER.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.removeProjectById(USUAL_USER.getId(), NON_EXISTING_PROJECT_ID);
        });
        service.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        service.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), USUAL_TASK2.getId());
        service.removeProjectById(USUAL_USER.getId(), USUAL_PROJECT1.getId());
        Assert.assertNull(projectRepository.findOneById(USUAL_PROJECT1.getId()));
        Assert.assertNull(taskRepository.findOneById(USUAL_TASK1.getId()));
        Assert.assertNull(taskRepository.findOneById(USUAL_TASK2.getId()));
    }

    @Test
    public void unbindTaskFromProject() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.unbindTaskFromProject(null, USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.unbindTaskFromProject("", USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.unbindTaskFromProject(USUAL_USER.getId(), null, USUAL_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.unbindTaskFromProject(USUAL_USER.getId(), "", USUAL_TASK1.getId());
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.unbindTaskFromProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), null);
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.unbindTaskFromProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.unbindTaskFromProject(USUAL_USER.getId(), NON_EXISTING_PROJECT_ID, USUAL_TASK1.getId());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.unbindTaskFromProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), NON_EXISTING_TASK_ID);
        });
        service.unbindTaskFromProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        Assert.assertNull(USUAL_TASK1.getProjectId());
        service.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
    }

}

