package ru.t1.nkiryukhin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.api.repository.IProjectRepository;
import ru.t1.nkiryukhin.tm.api.repository.ITaskRepository;
import ru.t1.nkiryukhin.tm.api.repository.IUserRepository;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;
import ru.t1.nkiryukhin.tm.api.service.IUserService;
import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.field.*;
import ru.t1.nkiryukhin.tm.exception.user.AccessDeniedException;
import ru.t1.nkiryukhin.tm.exception.user.ExistsEmailException;
import ru.t1.nkiryukhin.tm.exception.user.ExistsLoginException;
import ru.t1.nkiryukhin.tm.exception.user.UserNotFoundException;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;
import ru.t1.nkiryukhin.tm.model.User;
import ru.t1.nkiryukhin.tm.repository.ProjectRepository;
import ru.t1.nkiryukhin.tm.repository.TaskRepository;
import ru.t1.nkiryukhin.tm.repository.UserRepository;

import java.util.Collections;

import static ru.t1.nkiryukhin.tm.data.ProjectTestData.ADMIN_PROJECT1;
import static ru.t1.nkiryukhin.tm.data.TaskTestData.ADMIN_TASK1;
import static ru.t1.nkiryukhin.tm.data.UserTestData.*;

@Category(UnitCategory.class)
public class UserServiceTest {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserService service = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Before
    public void before() {
        userRepository.add(USUAL_USER);
    }

    @After
    public void after() throws AccessDeniedException {
        userRepository.removeAll(USER_LIST);
    }

    @Test
    public void add() throws AbstractFieldException {
        Assert.assertNull(service.add(NULL_USER));
        Assert.assertNotNull(service.add(ADMIN_USER));
        @Nullable final User user = service.findOneById(ADMIN_USER.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_USER, user);
    }

    @Test
    public void addMany() throws AbstractFieldException {
        Assert.assertNotNull(service.add(ADMIN_USER_LIST));
        for (final User user : ADMIN_USER_LIST)
            Assert.assertEquals(user, service.findOneById(user.getId()));
    }

    @Test
    public void set() {
        @NotNull final IUserRepository userEmptyRepository = new UserRepository();
        @NotNull final IPropertyService propertyEmptyService = new PropertyService();
        @NotNull final IProjectRepository projectEmptyRepository = new ProjectRepository();
        @NotNull final ITaskRepository taskEmptyRepository = new TaskRepository();
        @NotNull final IUserService emptyService = new UserService(propertyEmptyService, userEmptyRepository, projectEmptyRepository, taskEmptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(ADMIN_USER_LIST);
        emptyService.set(USER_LIST);
        Assert.assertEquals(USER_LIST, emptyService.findAll());
    }

    @Test
    public void findAll() {
        @NotNull final IUserRepository userEmptyRepository = new UserRepository();
        @NotNull final IPropertyService propertyEmptyService = new PropertyService();
        @NotNull final IProjectRepository projectEmptyRepository = new ProjectRepository();
        @NotNull final ITaskRepository taskEmptyRepository = new TaskRepository();
        @NotNull final IUserService emptyService = new UserService(propertyEmptyService, userEmptyRepository, projectEmptyRepository, taskEmptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(ADMIN_USER_LIST);
        Assert.assertEquals(ADMIN_USER_LIST, emptyService.findAll());
    }

    @Test
    public void findByEmail() throws EmailEmptyException {
        Assert.assertThrows(EmailEmptyException.class, () -> {
            service.findByEmail(null);
        });
        Assert.assertThrows(EmailEmptyException.class, () -> {
            service.findByEmail("");
        });
        String email = USUAL_USER.getEmail();
        @Nullable final User user = service.findByEmail(email);
        Assert.assertNotNull(user);
        Assert.assertEquals(USUAL_USER, user);
    }

    @Test
    public void existsById() {
        Assert.assertFalse(service.existsById(""));
        Assert.assertFalse(service.existsById(null));
        Assert.assertFalse(service.existsById(NON_EXISTING_USER_ID));
        Assert.assertTrue(service.existsById(USUAL_USER.getId()));
    }

    @Test
    public void findOneById() throws AbstractFieldException {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById("");
        });
        Assert.assertNull(service.findOneById(NON_EXISTING_USER_ID));
        @Nullable final User user = service.findOneById(USUAL_USER.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USUAL_USER, user);
    }

    @Test
    public void findOneByIndex() throws AbstractFieldException {
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(-1);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(service.getSize());
        });
        final int index = service.findAll().indexOf(USUAL_USER);
        @Nullable final User user = service.findOneByIndex(index);
        Assert.assertNotNull(user);
        Assert.assertEquals(USUAL_USER, user);
    }

    @Test
    public void clear() {
        @NotNull final IUserRepository userEmptyRepository = new UserRepository();
        @NotNull final IPropertyService propertyEmptyService = new PropertyService();
        @NotNull final IProjectRepository projectEmptyRepository = new ProjectRepository();
        @NotNull final ITaskRepository taskEmptyRepository = new TaskRepository();
        @NotNull final IUserService emptyService = new UserService(propertyEmptyService, userEmptyRepository, projectEmptyRepository, taskEmptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(ADMIN_USER_LIST);
        emptyService.clear();
        Assert.assertEquals(0, emptyService.getSize());
    }

    @Test
    public void removeById() throws AbstractFieldException {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById("");
        });
        Assert.assertNull(service.removeById(NON_EXISTING_USER_ID));
        userRepository.add(ADMIN_USER);
        @Nullable final User removedUser = service.removeById(ADMIN_USER.getId());
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_USER, removedUser);
        Assert.assertNull(service.findOneById(ADMIN_USER.getId()));
    }

    @Test
    public void removeByIndex() throws AbstractFieldException {
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(-1);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(service.getSize());
        });
        @Nullable final User createdUser = service.add(ADMIN_USER);
        final int index = service.findAll().indexOf(createdUser);
        @Nullable final User removedUser = service.removeByIndex(index);
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_USER, removedUser);
        Assert.assertNull(service.findOneById(ADMIN_USER.getId()));
    }

    @Test
    public void getSize() {
        @NotNull final IUserRepository userEmptyRepository = new UserRepository();
        @NotNull final IPropertyService propertyEmptyService = new PropertyService();
        @NotNull final IProjectRepository projectEmptyRepository = new ProjectRepository();
        @NotNull final ITaskRepository taskEmptyRepository = new TaskRepository();
        @NotNull final IUserService emptyService = new UserService(propertyEmptyService, userEmptyRepository, projectEmptyRepository, taskEmptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        Assert.assertEquals(0, emptyService.getSize());
        emptyService.add(ADMIN_USER);
        Assert.assertEquals(1, emptyService.getSize());
    }

    @Test
    public void removeAll() throws AccessDeniedException {
        @NotNull final IUserRepository userEmptyRepository = new UserRepository();
        @NotNull final IPropertyService propertyEmptyService = new PropertyService();
        @NotNull final IProjectRepository projectEmptyRepository = new ProjectRepository();
        @NotNull final ITaskRepository taskEmptyRepository = new TaskRepository();
        @NotNull final IUserService emptyService = new UserService(propertyEmptyService, userEmptyRepository, projectEmptyRepository, taskEmptyRepository);
        Assert.assertThrows(AccessDeniedException.class, () -> {
            emptyService.removeAll(null);
        });
        Assert.assertThrows(AccessDeniedException.class, () -> {
            emptyService.removeAll(Collections.emptyList());
        });
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(ADMIN_USER_LIST);
        emptyService.removeAll(ADMIN_USER_LIST);
        Assert.assertEquals(0, emptyService.getSize());
    }

    @Test
    public void create() throws AbstractException {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create(null, ADMIN_USER_PASSWORD);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create("", ADMIN_USER_PASSWORD);
        });
        Assert.assertThrows(ExistsLoginException.class, () -> {
            service.create(USUAL_USER_LOGIN, ADMIN_USER_PASSWORD);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_USER_LOGIN, null);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_USER_LOGIN, "");
        });
        @NotNull final User user = service.create(ADMIN_USER_LOGIN, ADMIN_USER_PASSWORD);
        Assert.assertEquals(user, service.findOneById(user.getId()));
        Assert.assertEquals(ADMIN_USER_LOGIN, user.getLogin());
        Assert.assertEquals(ADMIN_USER.getPasswordHash(), user.getPasswordHash());
    }

    @Test
    public void createWithEmail() throws AbstractException {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create(null, ADMIN_USER_PASSWORD, ADMIN_USER_EMAIL);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create("", ADMIN_USER_PASSWORD, ADMIN_USER_EMAIL);
        });
        Assert.assertThrows(ExistsLoginException.class, () -> {
            service.create(USUAL_USER_LOGIN, ADMIN_USER_PASSWORD, ADMIN_USER_EMAIL);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_USER_LOGIN, null, ADMIN_USER_EMAIL);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_USER_LOGIN, "", ADMIN_USER_EMAIL);
        });
        Assert.assertThrows(ExistsEmailException.class, () -> {
            service.create(ADMIN_USER_LOGIN, ADMIN_USER_PASSWORD, USUAL_USER_EMAIL);
        });
        @NotNull final User user = service.create(ADMIN_USER_LOGIN, ADMIN_USER_PASSWORD, ADMIN_USER_EMAIL);
        Assert.assertEquals(user, service.findOneById(user.getId()));
        Assert.assertEquals(ADMIN_USER_LOGIN, user.getLogin());
        Assert.assertEquals(ADMIN_USER.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(ADMIN_USER_EMAIL, user.getEmail());
    }

    @Test
    public void createWithRole() throws AbstractException {
        @NotNull final Role role = Role.ADMIN;
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create(null, ADMIN_USER_PASSWORD, role);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create("", ADMIN_USER_PASSWORD, role);
        });
        Assert.assertThrows(ExistsLoginException.class, () -> {
            service.create(USUAL_USER_LOGIN, ADMIN_USER_PASSWORD, role);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_USER_LOGIN, null, role);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_USER_LOGIN, "", role);
        });
        Assert.assertThrows(RoleEmptyException.class, () -> {
            @NotNull final Role nullRole = null;
            service.create(ADMIN_USER_LOGIN, ADMIN_USER_PASSWORD, nullRole);
        });
        @NotNull final User user = service.create(ADMIN_USER_LOGIN, ADMIN_USER_PASSWORD, Role.ADMIN);
        Assert.assertEquals(user, service.findOneById(user.getId()));
        Assert.assertEquals(ADMIN_USER_LOGIN, user.getLogin());
        Assert.assertEquals(ADMIN_USER.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    public void findByLogin() throws LoginEmptyException {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.findByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.findByLogin("");
        });
        @Nullable final User user = service.findByLogin(USUAL_USER_LOGIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(USUAL_USER, user);
    }



    @Test
    public void remove() throws AbstractFieldException, AccessDeniedException {
        Assert.assertNull(service.removeOne(null));
        @Nullable final User createdUser = service.add(ADMIN_USER);
        projectRepository.add(ADMIN_PROJECT1);
        taskRepository.add(ADMIN_TASK1);
        @Nullable final User removedUser = service.removeOne(createdUser);
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_USER, removedUser);
        Assert.assertNull(service.findOneById(ADMIN_USER.getId()));
        Assert.assertNull(projectRepository.findOneById(ADMIN_PROJECT1.getId()));
        Assert.assertNull(taskRepository.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeByLogin() throws AbstractException {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.removeByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.removeByLogin("");
        });
        Assert.assertNull(service.removeByLogin(NON_EXISTING_USER_ID));
        service.add(ADMIN_USER);
        service.removeByLogin(ADMIN_USER_LOGIN);
        Assert.assertNull(service.findOneById(ADMIN_USER.getId()));
    }

    @Test
    public void removeByEmail() throws AbstractException {
        Assert.assertThrows(EmailEmptyException.class, () -> {
            service.removeByEmail(null);
        });
        Assert.assertThrows(EmailEmptyException.class, () -> {
            service.removeByEmail("");
        });
        Assert.assertThrows(UserNotFoundException.class, () -> {
            service.removeByEmail(NON_EXISTING_USER_ID);
        });
        service.add(ADMIN_USER);
        service.removeByEmail(ADMIN_USER_EMAIL);
        Assert.assertNull(service.findOneById(ADMIN_USER.getId()));
    }

    @Test
    public void setPassword() throws AbstractException {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.setPassword(null, ADMIN_USER_PASSWORD);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.setPassword("", ADMIN_USER_PASSWORD);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.setPassword(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.setPassword(USUAL_USER.getId(), "");
        });
        Assert.assertThrows(UserNotFoundException.class, () -> {
            service.setPassword(NON_EXISTING_USER_ID, ADMIN_USER_PASSWORD);
        });
        service.setPassword(USUAL_USER.getId(), ADMIN_USER_PASSWORD);
        Assert.assertEquals(ADMIN_USER.getPasswordHash(), USUAL_USER.getPasswordHash());
        service.setPassword(USUAL_USER.getId(), USUAL_USER_PASSWORD);
    }

    @Test
    public void isLoginExists() {
        Assert.assertFalse(service.isLoginExist(null));
        Assert.assertFalse(service.isLoginExist(""));
        Assert.assertTrue(service.isLoginExist(USUAL_USER_LOGIN));
    }

    @Test
    public void isEmailExists() {
        Assert.assertFalse(service.isEmailExist(null));
        Assert.assertFalse(service.isEmailExist(""));
        String email = USUAL_USER.getEmail();
        Assert.assertTrue(service.isEmailExist(email));
    }

    @Test
    public void lockUserByLogin() throws AbstractException {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.lockUserByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.lockUserByLogin("");
        });
        Assert.assertThrows(UserNotFoundException.class, () -> {
            service.lockUserByLogin(NON_EXISTING_USER_ID);
        });
        service.add(ADMIN_USER);
        service.lockUserByLogin(ADMIN_USER_LOGIN);
        Assert.assertTrue(ADMIN_USER.getLocked());
    }

    @Test
    public void unlockUserByLogin() throws AbstractException {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.unlockUserByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.unlockUserByLogin("");
        });
        Assert.assertThrows(UserNotFoundException.class, () -> {
            service.unlockUserByLogin(NON_EXISTING_USER_ID);
        });
        service.add(ADMIN_USER);
        service.lockUserByLogin(ADMIN_USER_LOGIN);
        service.unlockUserByLogin(ADMIN_USER_LOGIN);
        Assert.assertFalse(ADMIN_USER.getLocked());
    }

    @Test
    public void updateUser() throws AbstractException {
        @NotNull final String firstName = "firstName";
        @NotNull final String lastName = "lastName";
        @NotNull final String middleName = "middleName";
        @NotNull final String email = "email";
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateUser(null, firstName, lastName, middleName, email);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateUser("", firstName, lastName, middleName, email);
        });
        service.updateUser(USUAL_USER.getId(), firstName, lastName, middleName, email);
        Assert.assertEquals(firstName, USUAL_USER.getFirstName());
        Assert.assertEquals(lastName, USUAL_USER.getLastName());
        Assert.assertEquals(middleName, USUAL_USER.getMiddleName());
    }

}

