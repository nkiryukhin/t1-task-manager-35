package ru.t1.nkiryukhin.tm.data;

import org.jetbrains.annotations.NotNull;

public class UserTestData {

    @NotNull
    public final static String USUAL_USER_LOGIN = "login";

    @NotNull
    public final static String USUAL_USER_PASSWORD = "password";

    @NotNull
    public final static String USUAL_USER_EMAIL = "email";

    @NotNull
    public final static String USUAL_FIRST_NAME = "first name";

    @NotNull
    public final static String USUAL_LAST_NAME = "last name";

    @NotNull
    public final static String USUAL_MIDDLE_NAME = "middle name";

    @NotNull
    public final static String ADMIN_USER_LOGIN = "admin";

    @NotNull
    public final static String ADMIN_USER_PASSWORD = "sadmin";

}
