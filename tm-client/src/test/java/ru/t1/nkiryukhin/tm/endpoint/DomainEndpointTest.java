package ru.t1.nkiryukhin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.nkiryukhin.tm.api.endpoint.IDomainEndpoint;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;
import ru.t1.nkiryukhin.tm.dto.request.*;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.marker.IntegrationCategory;
import ru.t1.nkiryukhin.tm.service.PropertyService;

import static ru.t1.nkiryukhin.tm.data.UserTestData.ADMIN_USER_LOGIN;
import static ru.t1.nkiryukhin.tm.data.UserTestData.ADMIN_USER_PASSWORD;

@Category(IntegrationCategory.class)
public final class DomainEndpointTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final IDomainEndpoint domainEndpoint = IDomainEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @BeforeClass
    public static void setUp() throws AbstractException {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(ADMIN_USER_LOGIN);
        loginRequest.setPassword(ADMIN_USER_PASSWORD);
        adminToken = authEndpoint.login(loginRequest).getToken();
    }

    @Test
    public void backupLoadData() throws Exception {
        @NotNull final DataBackupLoadRequest request = new DataBackupLoadRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataBackup(request));
    }

    @Test
    public void backupSaveData() throws Exception {
        @NotNull final DataBackupSaveRequest request = new DataBackupSaveRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataBackup(request));
    }

    @Test
    public void base64LoadData() throws Exception {
        @NotNull final DataBase64LoadRequest request = new DataBase64LoadRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataBase64(request));
    }

    @Test
    public void base64SaveData() throws Exception {
        @NotNull final DataBase64SaveRequest request = new DataBase64SaveRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataBase64(request));
    }

    @Test
    public void binaryLoadData() throws Exception {
        @NotNull final DataBinaryLoadRequest request = new DataBinaryLoadRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataBinary(request));
    }

    @Test
    public void binarySaveData() throws Exception {
        @NotNull final DataBinarySaveRequest request = new DataBinarySaveRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataBinary(request));
    }

    @Test
    public void jsonLoadFasterXmlData() throws Exception {
        @NotNull final DataJsonLoadFasterXmlRequest request = new DataJsonLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataJsonFasterXml(request));
    }

    @Test
    public void jsonSaveFasterXmlData() throws Exception {
        @NotNull final DataJsonSaveFasterXmlRequest request = new DataJsonSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataJsonFasterXml(request));
    }

    @Test
    public void jsonLoadJaxBData() throws Exception {
        @NotNull final DataJsonLoadJaxBRequest request = new DataJsonLoadJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataJsonJaxB(request));
    }

    @Test
    public void jsonSaveJaxBData() throws Exception {
        @NotNull final DataJsonSaveJaxBRequest request = new DataJsonSaveJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataJsonJaxB(request));
    }

    @Test
    public void xmlLoadFasterXmlData() throws Exception {
        @NotNull final DataXmlLoadFasterXmlRequest request = new DataXmlLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataXmlFasterXml(request));
    }

    @Test
    public void xmlSaveFasterXmlData() throws Exception {
        @NotNull final DataXmlSaveFasterXmlRequest request = new DataXmlSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataXmlFasterXml(request));
    }

    @Test
    public void xmlLoadJaxBData() throws Exception {
        @NotNull final DataXmlLoadJaxBRequest request = new DataXmlLoadJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataXmlJaxB(request));
    }

    @Test
    public void xmlSaveJaxBData() throws Exception {
        @NotNull final DataXmlSaveJaxBRequest request = new DataXmlSaveJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataXmlJaxB(request));
    }

    @Test
    public void yamlLoadFasterXmlData() throws Exception {
        @NotNull final DataYamlLoadFasterXmlRequest request = new DataYamlLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataYamlFasterXml(request));
    }

    @Test
    public void yamlSaveFasterXmlData() throws Exception {
        @NotNull final DataYamlSaveFasterXmlRequest request = new DataYamlSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataYamlFasterXml(request));
    }

}
