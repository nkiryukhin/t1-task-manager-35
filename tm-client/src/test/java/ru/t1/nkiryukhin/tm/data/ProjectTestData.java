package ru.t1.nkiryukhin.tm.data;

import org.jetbrains.annotations.NotNull;

public class ProjectTestData {

    @NotNull
    public final static String PROJECT_ONE_NAME = "Project 1";

    @NotNull
    public final static String PROJECT_ONE_DESCRIPTION = "Project One Description";

    @NotNull
    public final static String PROJECT_TWO_NAME = "Project 2";

    @NotNull
    public final static String PROJECT_TWO_DESCRIPTION = "Project Two Description";

    @NotNull
    public final static String PROJECT_THREE_NAME = "Project 3";

    @NotNull
    public final static String PROJECT_THREE_DESCRIPTION = "Project Three Description";

}
